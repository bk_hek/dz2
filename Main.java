package unn;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Какую математематическую операцию хотите исползовать?? ");
        String operation = in.nextLine();
        System.out.print("Введите первое число: ");
        double number1 = in.nextDouble();
        System.out.print("Введите второе число: ");
        double number2 = in.nextDouble();

        double result;
        String error;
        switch (operation) {
            case "/" -> {
                result = number1 / number2;
                System.out.println("Ответ: " + result);
            }
            case "*" -> {
                result = number1 * number2;
                System.out.println("Ответ: " + result);
            }
            case "+" -> {
                result = number1 + number2;
                System.out.println("Ответ: " + result);
            }
            case "-" -> {
                result = number1 - number2;
                System.out.println("Ответ: " + result);
            }
            default -> {
                error = "Такой матиматической операции не существует";
                System.out.println(error);
            }
        }
    }
}
